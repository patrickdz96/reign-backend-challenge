import { MigrationInterface, QueryRunner } from 'typeorm'

export class addNewsTable1641420489062 implements MigrationInterface {
  name = 'addNewsTable1641420489062'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      // eslint-disable-next-line max-len
      `CREATE TABLE "news" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "author" character varying NOT NULL, "_tags" character varying array NOT NULL, "created_at" TIMESTAMP NOT NULL, "month" character varying NOT NULL, CONSTRAINT "PK_39a43dfcb6007180f04aff2357e" PRIMARY KEY ("id"))`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "news"`)
  }
}
