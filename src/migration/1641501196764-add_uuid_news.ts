import { MigrationInterface, QueryRunner } from 'typeorm'

export class addUuidNews1641501196764 implements MigrationInterface {
  name = 'addUuidNews1641501196764'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "news" ADD "uuid" uuid NOT NULL DEFAULT uuid_generate_v4()`)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "news" DROP COLUMN "uuid"`)
  }
}
