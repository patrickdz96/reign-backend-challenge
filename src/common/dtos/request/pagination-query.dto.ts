import { ApiProperty } from '@nestjs/swagger'
import { IsOptional, IsPositive } from 'class-validator'

export class PaginationQueryDto {
  @IsOptional()
  @IsPositive()
  @ApiProperty({ description: 'Number of the actual page' })
  page?: number = 1

  @IsOptional()
  @IsPositive()
  @ApiProperty({ description: 'Size of items to paginate' })
  perPage?: number = 5
}
