import { Exclude, Expose } from 'class-transformer'
import { NewsDTO } from '../../../news/dtos/response/news.dto'
import { PaginationDto } from '../response/pagination.dto'

@Exclude()
export class QueueCollectionDto {
  @Expose()
  data: NewsDTO[]

  @Expose()
  pageInfo: PaginationDto
}
