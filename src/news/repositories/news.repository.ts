import { Logger, UnprocessableEntityException } from '@nestjs/common'
import { plainToClass } from 'class-transformer'
import { EntityRepository, Repository } from 'typeorm'
import { PaginationDto } from '../../common/dtos/response/pagination.dto'
import { QueueCollectionDto } from '../../common/dtos/response/queue-collection.dto'
import { paginatedHelper, paginationSerializer } from '../../common/utils/pagination.utils'
import { GetNewsDto } from '../dtos/request/get-news.dto'
import { News } from '../entities/news.entity'

@EntityRepository(News)
export class NewsRepository extends Repository<News> {
  private readonly logger: Logger

  constructor() {
    super()
    this.logger = new Logger(NewsRepository.name)
  }

  async getAllNews(params: GetNewsDto): Promise<QueueCollectionDto> {
    const { page, perPage, title, author, tags, month } = params
    const { skip, take } = paginatedHelper(params)

    try {
      let query = this.createQueryBuilder('news')

      if (title) query = query.andWhere('news.title ilike :title', { title: `%${title}%` })
      if (author) query = query.andWhere('news.author ilike :author', { author: `%${author}%` })
      if (tags) query = query.andWhere('news._tags @> (:_tags)::varchar[]', { _tags: tags })
      if (month) query = query.andWhere('news.month = :month', { month: month })

      const [data, count] = await query.skip(skip).take(take).getManyAndCount()
      const pageInfo: PaginationDto = paginationSerializer(count, {
        page,
        perPage,
      })

      return plainToClass(QueueCollectionDto, { pageInfo, data })
    } catch (error) {
      this.logger.error(error)

      throw new UnprocessableEntityException(error)
    }
  }

  async deleteByUUID(uuid: string): Promise<News> {
    try {
      const news = await this.findOne({ uuid })
      await this.remove(news)

      return news
    } catch (error) {
      this.logger.error(error)

      throw new UnprocessableEntityException(error)
    }
  }
}
