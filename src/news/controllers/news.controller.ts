import { Controller, Delete, Get, Param, Post, Query, UseGuards } from '@nestjs/common'
import { ApiBearerAuth } from '@nestjs/swagger'
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard'
import { QueueCollectionDto } from '../../common/dtos/response/queue-collection.dto'
import { GetNewsDto } from '../dtos/request/get-news.dto'
import { NewsDTO } from '../dtos/response/news.dto'
import { News } from '../entities/news.entity'
import { NewsService } from '../services/news.service'

@Controller('api/news')
@UseGuards(JwtAuthGuard)
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Post('populate')
  @ApiBearerAuth()
  async populate(): Promise<NewsDTO[]> {
    return await this.newsService.populateNews()
  }

  @Get('/')
  @ApiBearerAuth()
  async getAllNews(@Query() params: GetNewsDto): Promise<QueueCollectionDto> {
    return await this.newsService.getAllNews(params)
  }

  @Delete(':uuid')
  @ApiBearerAuth()
  async deleteByUUID(@Param('uuid') uuid: string): Promise<News> {
    return await this.newsService.deleteByUUID(uuid)
  }
}
