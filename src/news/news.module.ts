import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { NewsController } from './controllers/news.controller'
import { NewsRepository } from './repositories/news.repository'
import { NewsService } from './services/news.service'

@Module({
  imports: [TypeOrmModule.forFeature([NewsRepository])],
  controllers: [NewsController],
  providers: [NewsService],
  exports: [NewsService],
})
export class NewsModule {}
