import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsOptional } from 'class-validator'
import { PaginationQueryDto } from '../../../common/dtos/request/pagination-query.dto'
import { MonthsEnum } from '../../enums/news.enum'

export class GetNewsDto extends PaginationQueryDto {
  @IsOptional()
  @ApiProperty({ description: 'Title of the news' })
  title?: string

  @IsOptional()
  @ApiProperty({ description: 'Author of the news' })
  author?: string

  @IsOptional()
  @ApiProperty({ description: 'Tags of the news' })
  tags?: string[]

  @IsOptional()
  @ApiProperty({ description: 'Month of the news' })
  @IsEnum(MonthsEnum)
  month?: MonthsEnum
}
