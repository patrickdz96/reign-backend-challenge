import { ApiProperty } from '@nestjs/swagger'

export class NewsDTO {
  @ApiProperty()
  uuid: string

  @ApiProperty()
  title: string

  @ApiProperty()
  author: string

  @ApiProperty()
  _tags: string[]

  @ApiProperty()
  created_at: Date

  @ApiProperty()
  month: string
}
