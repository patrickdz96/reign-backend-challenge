import { Injectable, Logger, UnprocessableEntityException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import axios from 'axios'
import { QueueCollectionDto } from '../../common/dtos/response/queue-collection.dto'
import { GetNewsDto } from '../dtos/request/get-news.dto'
import { News } from '../entities/news.entity'
import { NewsRepository } from '../repositories/news.repository'

@Injectable()
export class NewsService {
  private readonly logger: Logger

  constructor(
    @InjectRepository(NewsRepository)
    private newsRepository: NewsRepository,
  ) {}

  async getNews() {
    const response = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
    const newsData = response.data.hits

    return newsData
  }

  async populateNews(): Promise<News[]> {
    try {
      const newsData = await this.getNews()

      const createdNews = await Promise.all(
        newsData.map(async (hit) => {
          const newHit = new News()
          newHit.title = hit.story_title ? hit.story_title : hit._highlightResult.title.value
          newHit.author = hit.author
          newHit._tags = hit._tags
          newHit.created_at = hit.created_at
          newHit.month = this.getMonthFromDate(new Date(hit.created_at))

          return this.newsRepository.save(newHit)
        }),
      )

      return createdNews
    } catch (error) {
      this.logger.error(error)

      throw new UnprocessableEntityException(error)
    }
  }

  async getAllNews(params: GetNewsDto): Promise<QueueCollectionDto> {
    return this.newsRepository.getAllNews(params)
  }

  private getMonthFromDate(date: Date) {
    return date.toLocaleString('en-US', { month: 'long' }).toLowerCase()
  }

  deleteByUUID(uuid: string): Promise<News> {
    try {
      return this.newsRepository.deleteByUUID(uuid)
    } catch (error) {
      this.logger.error(error)

      throw new UnprocessableEntityException(error)
    }
  }
}
