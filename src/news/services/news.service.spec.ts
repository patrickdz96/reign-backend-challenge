import { Test, TestingModule } from '@nestjs/testing'
import { TypeOrmModule } from '@nestjs/typeorm'
import * as faker from 'faker'
import * as typeOrmConfig from '../../typeorm.config'
import { News } from '../entities/news.entity'
import { MonthsEnum } from '../enums/news.enum'
import { NewsRepository } from '../repositories/news.repository'
import { NewsService } from './news.service'

describe('NewsService', () => {
  let newsService: NewsService
  let newsRepository: NewsRepository

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NewsService],
      imports: [TypeOrmModule.forRoot(typeOrmConfig), TypeOrmModule.forFeature([NewsRepository])],
    }).compile()

    newsService = module.get<NewsService>(NewsService)
    newsRepository = module.get<NewsRepository>(NewsRepository)
  })

  it('should be defined', () => {
    expect(newsService).toBeDefined()
  })

  describe('getAllNews', () => {
    //const newHits: Array<News>

    const newHits: News[] = []

    beforeAll(async () => {
      for (let i = 0; i < 5; i++) {
        const newHit = new News()
        newHit.title = faker.name.title()
        newHit.author = `${faker.name.firstName()} ${faker.name.lastName()}`
        newHit.month = faker.date.month().toLowerCase()
        newHit._tags = [faker.random.word(6), faker.random.word(6), faker.random.word(6)]
        newHit.created_at = faker.datatype.datetime()

        const createdHit = await newsRepository.save(newHit)
        newHits.push(createdHit)
      }
    })

    it('should get all news matching param -title', async () => {
      const params = {
        page: 1,
        perPage: 5,
        title: newHits[0].title,
      }

      const result = await newsService.getAllNews(params)

      expect(result.data.length).toBeGreaterThanOrEqual(1)
    })

    it('should get all news matching param -author', async () => {
      const params = {
        page: 1,
        perPage: 5,
        author: newHits[0].author,
      }

      const result = await newsService.getAllNews(params)

      expect(result.data.length).toBeGreaterThanOrEqual(1)
    })

    it('should get all news matching param -month', async () => {
      const params = {
        page: 1,
        perPage: 5,
        month: newHits[0].month as MonthsEnum,
      }

      const result = await newsService.getAllNews(params)

      expect(result.data.length).toBeGreaterThanOrEqual(1)
    })

    it('should get all news matching param -tags', async () => {
      const params = {
        page: 1,
        perPage: 5,
        tags: newHits[0]._tags,
      }

      const result = await newsService.getAllNews(params)

      expect(result.data.length).toBeGreaterThanOrEqual(1)
    })

    it('should get empty array when news do not match params -tags', async () => {
      const params = {
        page: 1,
        perPage: 5,
        tags: [faker.random.word(5), faker.random.word(5)],
      }

      const result = await newsService.getAllNews(params)

      expect(result.data).toHaveProperty('length', 0)
    })
  })

  describe('deleteByUUID', () => {
    const newHit = new News()

    beforeAll(async () => {
      newHit.title = faker.name.title()
      newHit.author = `${faker.name.firstName()} ${faker.name.lastName()}`
      newHit.month = faker.date.month()
      newHit._tags = [faker.random.words(9), faker.random.words(6), faker.random.words(4)]
      newHit.created_at = faker.datatype.datetime()

      await newsRepository.save(newHit)
    })

    it('should delete news by uuid', async () => {
      const uuid = newHit.uuid

      const result = await newsService.deleteByUUID(uuid)

      expect(result).toHaveProperty('uuid', uuid)
    })

    it('should throw error with invalid uuid', async () => {
      const uuid = faker.datatype.uuid()

      await expect(newsService.deleteByUUID(uuid)).rejects.toThrowErrorMatchingSnapshot()
    })
  })
})
