import { Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { AuthData } from '../types/auth.type'

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  signIn(): AuthData {
    return { token: this.jwtService.sign({}) }
  }
}
