import { Test, TestingModule } from '@nestjs/testing'
import { JwtModule } from '@nestjs/jwt'
import { JwtService } from '@nestjs/jwt'
import { AuthService } from './auth.service'

describe('AuthService', () => {
  let authService: AuthService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthService],
      imports: [
        JwtModule.register({
          secret: process.env.JWT_SECRET,
          signOptions: { expiresIn: process.env.JWT_EXPIRES_IN },
        }),
      ],
    }).compile()

    authService = module.get<AuthService>(AuthService)
  })

  it('should be defined', () => {
    expect(authService).toBeDefined()
  })

  describe('signIn', () => {
    it('should create a token', async () => {
      const spy = jest.spyOn(JwtService.prototype, 'sign').mockImplementation(() => 'my.jwt.token')

      const token = await authService.signIn()

      expect(token).toHaveProperty('token', 'my.jwt.token')
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })
})
