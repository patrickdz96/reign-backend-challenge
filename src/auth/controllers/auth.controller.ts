import { Controller, Get } from '@nestjs/common'
import { AuthService } from '../services/auth.service'

@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Get('sign-in')
  signIn() {
    return this.authService.signIn()
  }
}
