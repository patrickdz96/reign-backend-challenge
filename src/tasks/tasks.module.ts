import { Module } from '@nestjs/common'
import { NewsModule } from '../news/news.module'
import { TasksService } from './services/task.service'

@Module({
  providers: [TasksService],
  imports: [NewsModule],
})
export class TasksModule {}
