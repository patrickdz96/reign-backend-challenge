import { Injectable, Logger } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import { NewsService } from '../../news/services/news.service'

@Injectable()
export class TasksService {
  private readonly logger: Logger

  constructor(private newService: NewsService) {
    this.logger = new Logger(TasksService.name)
  }

  @Cron(CronExpression.EVERY_HOUR)
  handleCron() {
    this.logger.debug('Populating...')
    this.newService.populateNews()
  }
}
