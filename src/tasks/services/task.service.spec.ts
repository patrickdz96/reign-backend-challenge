import { Test, TestingModule } from '@nestjs/testing'
import { TypeOrmModule } from '@nestjs/typeorm'
import { NewsModule } from '../../news/news.module'
import * as typeOrmConfig from '../../typeorm.config'
import { TasksService } from './task.service'

describe('TasksService', () => {
  let taskService: TasksService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TasksService],
      imports: [TypeOrmModule.forRoot(typeOrmConfig), NewsModule],
    }).compile()

    taskService = module.get<TasksService>(TasksService)
  })

  it('should be defined', () => {
    expect(taskService).toBeDefined()
  })
})
