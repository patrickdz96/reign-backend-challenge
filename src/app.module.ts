import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ScheduleModule } from '@nestjs/schedule'
import { AuthModule } from './auth/auth.module'
import { NewsModule } from './news/news.module'
import { TasksModule } from './tasks/tasks.module'
import * as typeOrmConfig from './typeorm.config'

@Module({
  imports: [TypeOrmModule.forRoot(typeOrmConfig), AuthModule, NewsModule, ScheduleModule.forRoot(), TasksModule],
})
export class AppModule {}
