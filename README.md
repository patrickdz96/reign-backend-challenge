<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

# Reign Backend Challenge

## Description

This project is a challenge to create a REST-API using Nest framework with TypeORM.

## Built with

- NodeJS v16.13
- NestJS
- Docker
- TypeORM
- PostgreSQL
- Swagger
- Linters
- Prettier
- Husky

## Prerequisites

Install the following dependency:

- [Docker](https://docs.docker.com/get-docker/)

## Installation

### Environment variables

1. create a copy of .env.test.example with the name `.env.test`, placed it at the root of the project, and replace its values with your own credentials.

```
DB_NAME=
DB_USERNAME=
DB_PASSWORD=
JWT_SECRET=
JWT_EXPIRES_IN=
```

2. Run the following commands inside the project:

```bash
# build the project
$ docker-compose build

# lint your code before commit, test your code before push
$ docker-compose run api npm run husky:prepare
```

## Database set up

```bash
# migrate database
$ docker-compose run api npm run typeorm:run
```

## Running the app

```bash
$ docker-compose up
```

# Test

## Prerequisites

Install the following dependencies:

- [Volta](https://volta.sh/) (node, npm, and yarn version manager)
- [PostgreSQL](https://www.postgresql.org/download/)

## Installation

1. create a copy of .env.example with the name `.env.test`, placed it at the root of the project, and replace its values with your own credentials.

```
POSTGRES_HOST=
DB_NAME=
DB_USERNAME=
DB_PASSWORD=
JWT_SECRET=
JWT_EXPIRES_IN=
```

2. Run the following commands inside the project:

```bash
$ volta install node

$ npm install

# migrate database tests
$ npm run typeorm-test:run

# unit tests
$ npm run test

# test coverage
$ npm run test:cov

```

# Additional Information

- You can find the collection of API requests in the postman-collection folder

- After configure the project you can populate the database following the next steps:

  1. Generate token with GET request http://localhost:8000/api/auth/sign-in

  2. Populate database with POST request http://localhost:8000/api/news/populate

- Finally, you can find the swagger api doc here http://localhost:8000/api/docs

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
